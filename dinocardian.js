// ==UserScript==
// @name         Dinocardian
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to save your cards!
// @author       Biosha
// @match        http://www.dinocard.net/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=dinocard.net
// @grant GM_xmlhttpRequest
// ==/UserScript==

const serverURL = 'http://localhost:3333'
const userId = document.getElementById("usermenu").children[0].children[0].attributes.href.value.match(/id=(\d*)/)[1]

function addButton() {
    let update_external_tools_btn = document.getElementById('save');
    if (update_external_tools_btn) return;

    const usermenu = document.getElementById('usermenu');


    //Bouton save Collection
    if (window.location.pathname === '/card/viewAll') {
        const endcollect = document.getElementById('center').children[6]
        let btn = document.createElement('a');
        btn.innerHTML = 'Sauvegarder la collection'
        btn.id = 'save';
        btn.className = "button";
        btn.addEventListener("click", saveCollection);
        document.getElementById('center').insertBefore(btn, endcollect);
    }

    //Bouton save Deck
    if (window.location.pathname === '/pack/info') {
        const rightMenu = document.getElementById('rightMenu')
        let btn = document.createElement('a');
        btn.innerHTML = 'Sauvegarder le deck'
        btn.id = 'save';
        btn.className = "button";
        btn.addEventListener("click", saveDeck);
        rightMenu.appendChild(btn);
    }

}


function sendToServer(page, data) {
    fetch(`${serverURL}/${page}/${userId}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
}


function saveCollection(){
    if (document.getElementById("folder2").classList[0] === 'disable') {
        window.alert("Mauvais onglet sélectionné\nMerci d'être en mode tableau")
        return
    }
    let cardArray = []

    const collection = document.getElementById("superTab").children[0].children[0].children
    for (let index = 1; index < collection.length; index++) {
        const card = collection[index].getElementsByClassName("cardName")[0];
        const quantity = parseInt(card.innerText.match(/\d*/)[0]);
        const cardId = parseInt(card.children[0].attributes.id.value.match(/name(\d*)/)[1]);
        cardArray.push({cardId: cardId, quantity: quantity})
    }
    sendToServer('collection', cardArray)
}

function saveDeck(){
    if (document.getElementById("folder2").classList[0] === 'disable') {
        window.alert("Mauvais onglet sélectionné\nMerci d'être en mode tableau")
        return
    }
    let cardArray = []

    const collection = document.getElementById("superTab").children[0].children[0].children
    for (let index = 1; index < collection.length; index++) {
        const card = collection[index].getElementsByClassName("cardName")[0];
        const quantity = parseInt(card.innerText.match(/\d*/)[0]);
        const cardId = parseInt(card.children[0].attributes.href.value.match(/id=(\d*)/)[1]);
        cardArray.push({cardId: cardId, quantity: quantity})
    }
    const infoPack = document.getElementsByClassName('infoPack')[0]
    const deckInfo = {
        deckId: window.location.search.match(/\?id=(\d*)/)[1],
        level: parseInt(infoPack.getElementsByClassName('levelBox')[0].textContent),
        victory: parseInt(infoPack.getElementsByClassName('victoryBox')[0].textContent.match(/Victoire : (\d*)%/)[1]),
        experience: parseInt(infoPack.getElementsByClassName('xpBox')[0].textContent.match(/Expérience : (\d*)%/)[1]),
        totalCard: parseInt(infoPack.getElementsByClassName('packName')[0].innerText.match(/.*\[ (\d*) cartes \].*/)[1]),
        perception: parseInt(document.getElementById("4").innerText),
        strategy: parseInt(document.getElementById("3").innerText),
        bonus: parseInt(document.getElementById("1").innerText),
        life: parseInt(document.getElementById("2").innerText),
        description: document.getElementById("desc").innerText,
        dateCreated: infoPack.getElementsByClassName('packName')[0].getElementsByTagName('span')[1].innerText,
        name: infoPack.getElementsByClassName('packName')[0].getElementsByTagName('span')[0].innerText,
        cards: cardArray
    }
    console.log(deckInfo)


    sendToServer('deck', deckInfo)
}

(function() {
    setTimeout(() => {
        addButton()
    }, 2000);

})();
