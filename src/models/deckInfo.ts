import { Cards } from "./card";

export interface Deck {
    deckId: number,
    victory: number,
    experience: number,
    totalCard: number,
    perception: number,
    strategy: number,
    bonus: number,
    life: number,
    name: string,
    description: string,
    dateCreated: string,
    cards: Array<Cards>
}