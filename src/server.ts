import app from './app';
import { AppDataSource } from './data-source';



AppDataSource.initialize()
	.then(() => {
		console.log('Data Source has been initialized successfully.');
        app.listen(3333);
	})
	.catch(err => {
		console.error('Error during Data Source initialization:', err);
	});