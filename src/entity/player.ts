import {
	Column,
	Entity,
	OneToMany,
	PrimaryGeneratedColumn,
	Relation
} from 'typeorm';
import { Deck } from './deck';
import { Cards } from './collection';

@Entity()
export class Player {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({
		nullable: true
	})
	userId: number; //OK

	@Column({
		nullable: true
	})
	name: string; //OK

	
	@OneToMany(() => Deck, deck => deck.player, {
		cascade: true
	})
	deck: Relation<Deck[]>;

	@OneToMany(() => Cards, cards => cards.player, {
		cascade: true
	})
	cards: Relation<Cards[]>;


	constructor(userId: number) {
		this.userId = userId;
	}
}
