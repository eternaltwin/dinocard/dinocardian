import { Entity, PrimaryGeneratedColumn, ManyToOne, Relation, Column } from 'typeorm';
import { Player } from './player';

@Entity()
export class Cards {
	@PrimaryGeneratedColumn()
	id: number;

    @ManyToOne(() => Player, player => player.cards, {
		onDelete: 'CASCADE'
	})
	player: Relation<Player>;

    @Column()
    cardId: number;

    @Column()
    quantity: number;
}