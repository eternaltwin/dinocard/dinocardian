import { Entity, PrimaryGeneratedColumn, ManyToOne, Relation, Column } from 'typeorm';
import { Player } from './player';

@Entity()
export class Deck {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(() => Player, player => player.deck, {
		onDelete: 'CASCADE'
	})
	player: Relation<Player>;

	@Column()
	deckId: number; //OK

	@Column()
	victory: number; //OK

	@Column()
	experience: number; //OK

	@Column()
	totalCard: number; //OK

	@Column()
	perception: number; //OK

	@Column()
	strategy: number; //OK

	@Column()
	bonus: number; //OK

	@Column()
	life: number; //OK

	@Column()
	name: number; //OK

	@Column()
	description: number; //OK

	@Column()
	dateCreated: number; //OK

	@Column('text', {
		nullable: true,
		array: true
	})
	cards: Array<string>; //OK

	constructor(player: Player) {
		this.player = player;
	}
}
