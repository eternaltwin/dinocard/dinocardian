import fs from 'fs';
import toml from 'toml';

let config: DbConfig;

const getEnvironnement = (): string => {
	return process.env.NODE_ENV ?? 'development';
};

const loadConfigFile = (): void => {
	config = toml.parse(fs.readFileSync(`./config_${getEnvironnement()}.toml`, 'utf-8'));
};

const getConfig = (): DbConfig => {
	return config;
};

interface DbConfig {
	readonly host: string;
	readonly user: string;
	readonly password: string;
	readonly dbName: string;
}

export { getEnvironnement, loadConfigFile, getConfig, DbConfig };
