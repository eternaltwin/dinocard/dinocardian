import { Request, Response, Router } from 'express';
import { param, validationResult } from 'express-validator';
import { Cards } from './models/card';
import { Deck } from './models/deckInfo';

const routes = Router();

routes.get('/test', (req, res) => {
    console.log(req.body)
  return res.json({ message: 'Hello World' });
});

routes.put(
	`/collection/:id`,
	[param('id').exists().toInt().isNumeric()],
	async (req: Request, res: Response) => {
		if (!validationResult(req).isEmpty()) {
			return res.status(400).json({ errors: validationResult(req) });
		}
    const collection = req.body as Array<Cards>
    console.log(collection)

		try {
			// const response: DinozFiche = await getDinozFiche(req);
			// return res.status(200).send(response);
		} catch (err) {
			// const e = err as ErrorFormator;
			// await postError(e, res);
			// res.status(e.errorCode).send(e.message);
		}
	}
);

routes.put(
	`/deck/:id`,
	[param('id').exists().toInt().isNumeric()],
	async (req: Request, res: Response) => {
		if (!validationResult(req).isEmpty()) {
			return res.status(400).json({ errors: validationResult(req) });
		}
    const collection = req.body as Deck
    console.log(collection)

		try {
			// const response: DinozFiche = await getDinozFiche(req);
			// return res.status(200).send(response);
		} catch (err) {
			// const e = err as ErrorFormator;
			// await postError(e, res);
			// res.status(e.errorCode).send(e.message);
		}
	}
);

export default routes;